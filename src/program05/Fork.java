/**
 * Group Names: Erik Pond / Steven Laabs
 */
package program05;

/**
 * Fork class is used to create the forks and determine if they are 
 * currently being held by the philosophers.  Only two forks per philosopher.
 * @author laabss
 */
public class Fork 
{
    int forkID;
    public boolean isHeld;
    
    /**
     * Constructor for the forks, initializing them to not being held
     * @param id is what forks belong to what philosophers
     */
    public Fork(int id)
    {
        isHeld = false;
        forkID = id;
    }
    
    /**
     * Picks up the forks that were set down unless already holding
     * @return true if the forks are not already held, false otherwise
     */
    public boolean pickUp()
    {
       if(!isHeld)
       {
         isHeld = true;
         return true;
       }
       
       return false;
    }
    
    /**
     * Sets down the forks to allow others to pick them up.
     */
    public void setDown()
    {
        isHeld = false;
    }
    
}
