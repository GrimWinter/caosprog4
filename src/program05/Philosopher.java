/**
 * Group Names: Erik Pond / Steven Laabs
 */
package program05;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Philosopher class represents a philosopher, what they
 * are holding and doing in between thinking and eating.
 * They try to grab the forks and if they can not then
 * they simply continue thinking.
 * @author ponde / laabs
 */
public class Philosopher extends Thread
{
    int id;
    Fork leftFork;
    Fork rightFork;
    
    Random random;
    long timeThinking = 0;
    long timeEating = 0;
    int timesAte = 0;
    
    boolean isPhilosophising = true;

    /**
     * The status of the philosophers to determine what to do.
     */
    public enum Status
    {
        THINKING, WAITING_LEFT, WAITING_RIGHT, EATING
    }
    
    Status currentStatus = Status.THINKING;
    
    /**
     * Constructor for the philosopher to create an id and set their forks.
     * Creator: Erik
     * @param id is the identification number of the philosopher.
     * @param lfork is what fork id the philosopher has for their left hand.
     * @param rfork is what fork id the philosopher has for their right hand.
     */
    public Philosopher(int id, Fork lfork, Fork rfork)
    {
        this.id = id;
        this.leftFork = lfork;
        this.rightFork = rfork;
        
        random = new Random();
    }
    
    /**
     * Runs the program, every random number of seconds eating, the 
     * philosopher will put down the forks and think for a random number
     * of seconds as well. While thinking other philosophers try to 
     * pick up the forks to eat and stop thinking.  Never allows for two
     * philosophers to use the same forks.
     *  Creator: Steven
     */
    @Override
    public void run()
    {
       while(isPhilosophising)
       {
          try 
          {
             /**
              *  Creator: Erik
              */
             System.out.println(id + " THINKING");
             currentStatus = Status.THINKING;
             // think for random number of milliseconds <=1 sec
             long think = random.nextInt(100) + 1;
             timeThinking += think;
             Thread.sleep(think);
          } 
          
          /**
           *  Creator: Steven
           */
          catch (InterruptedException ex)
          {
             Logger.getLogger(Philosopher.class.getName()).log(Level.SEVERE, null, ex);
          }

          while(!rightFork.pickUp())
          {
             currentStatus = Status.WAITING_RIGHT;
          }
          
          while(!leftFork.pickUp())
          {
             currentStatus = Status.WAITING_LEFT;
          }
          
          /**
           *  Creator: Erik
           */
          try 
          {
             System.out.println(id + " EATING");
             currentStatus = Status.EATING;
             timesAte += 1;
             long eat = random.nextInt(100) + 1;
             timeEating += eat;
             Thread.sleep(eat);
          } 
          
          /**
           *  Creator: Steven
           */
          catch (InterruptedException ex) 
          {
             Logger.getLogger(Philosopher.class.getName()).log(Level.SEVERE, null, ex);
          }
          
          leftFork.setDown();
          rightFork.setDown();

       }
    }
    
    /**
     * Determines how many times a philosopher ate.
     * @return amount of times a philosopher ate.
     *  Creator: Steven
     */
    public int getTimesAte() { return timesAte; }
    
    /**
     * Determines how long a philosopher ate.
     * @return amount of time a philosopher ate.
     *  Creator: Steven
     */
    public long getTotalTimeEat() { return timeEating; }
    
    /**
     * Determines how long a philosopher thought.
     * @return amount of time a philosopher thought.
     *  Creator: Steven
     */
    public long getTotalTimeThink() { return timeThinking; }
    
    /**
     * Philosopher is done thinking and eating.
     *  Creator: Steven
     */
    public void terminate() { isPhilosophising = false; }
    
    /**
     * To string method to allow the stats of each philosopher to be
     * printed at the end of the program.
     *  Creator: Erik
     * @return philosophers id, how many times they ate, how long they
     * spent eating, and how long they thought for in between eating
     * cycles as a string.
     */
    @Override
    public String toString()
    {
        return "Philosopher #" + id + " ate " + timesAte
                + " times for " + timeEating + " milliseconds and"
                + " thought a total time of " 
                + timeThinking + " milliseconds.";
    }
}
