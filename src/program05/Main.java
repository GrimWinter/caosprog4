/**
 * Group Names: Erik Pond / Steven Laabs
 */
package program05;

import java.util.ArrayList;

/**
 * Main initiates and runs the program.  Setting up the number of
 * philosophers, forks, and assigning them correctly.  At end of execution
 * it prints out the statistics of the program.
 * @author laabss
 */
public class Main
{

    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException 
    {
        int numObjects = 5;
    
        ArrayList<Philosopher> philosophers = new ArrayList(numObjects);
        ArrayList<Fork> forks = new ArrayList(numObjects);
        
        /**
         * Create the number of forks needed for all the philosophers
         */
        for(int i = 0; i < numObjects; i++)
        {
            forks.add(new Fork(i));
        }
        
        /**
         * Create the philosophers and assign them the forks to the left
         * and to the right of them.
         */
        for(int i = 0; i < numObjects; i++)
        {
            philosophers.add(new Philosopher(i,
                             forks.get((i + 1 + numObjects) % numObjects),
                             forks.get(i)));
            
            philosophers.get(i).start();
        }
        
        Thread.sleep(3000);
        
        /**
         * Finished eating.
         */
        for(Philosopher p : philosophers)
        {
           p.terminate();
        }
        
        Thread.sleep(70 * numObjects);
        
        /**
         * Print out the output for the program to show that there
         * is no starvation for the philosophers.
         */
        for(Philosopher p : philosophers)
        {
           System.out.println(p.toString());
        }
    }
    
}
